package hr.fer.ruazosa.locationtrack.LocationTrackServices;

import java.util.List;

public interface IUserService {
    public void createNewUser(User user);
    public List<User> findAll();
    boolean checkIfUsernameIsTaken(User user);
    public boolean loginInUser(String username, String password);
}
