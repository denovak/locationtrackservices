package hr.fer.ruazosa.locationtrack.LocationTrackServices;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    @Query("SELECT id FROM User where userName = ?1")
    Long findByUserName(String userName);

    @Query("SELECT id FROM User where userName = ?1 and password = ?2")
    Long loginUser(String userName, String password);

}