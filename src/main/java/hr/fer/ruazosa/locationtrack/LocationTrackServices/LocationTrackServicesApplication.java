package hr.fer.ruazosa.locationtrack.LocationTrackServices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocationTrackServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(LocationTrackServicesApplication.class, args);
	}

}
